# ECU Centennial Digital Exhibit

Jekyll site for the ECU Centennial legacy digital exhibit.
* [http://digital.lib.ecu.edu/exhibits/ecucent](http://digital.lib.ecu.edu/exhibits/ecucent)

## Modification

Modification requires building the site with [Jekyll](https://jekyllrb.com/), which requires Ruby.

1. Clone repo
2. $ cd ecu-centennial
3. $ bundle install
4. $ bundle exec jekyll serve

This should make [http://localhost:4000/exhibits/ecucent/](http://localhost:4000/exhibits/ecucent/) available in your browser to view the site (trailing slash is important here). After changes are made, copy the contents of the _site directory to the live webserver.

